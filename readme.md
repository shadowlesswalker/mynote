# Documentation

Written by: ShadowlessWalker

### Preamble

Do you have troubles below:

- Don't remember ID or password of some sites or apps usually?
- To avoid forgetting passwords of different sites, you set the same password for different sites,which is extremely unsafe and easily hacked by some persons.
- To avoid forgetting some important things, such as passwords, you write them in a file that is readable and comprehensible  to others, which is also unsafe.

So I made this python program to access an encrypted data file that you can put some important records in. The data file is stored on disk after encrypting, so anyone can't understand the content even if they get the file.

The program also provides other useful functions , which will keep improving.

### Data File

The data file is a fixed format json string after decrypting. A typical example is as following:

```
{
    "meta": {
        "name": "mynote",
        "timestamp": "2022-04-09 07:57:34"
    },
    "data": {
        "account.a.com": {
            "account1": {
                "*": "Tom",
                "pwd": "654321",
                "timestamp": "2022-04-09 07:48:23"
            },
            "account2": {
                "*": "Jack",
                "pwd": "555666"
            }
        },
        "sites": {
            "a": "http://www.a.com",
            "b": "http://www.b.com"
        }
    }
}
```

All keys are optional except for keys bellow:

- "meta" and its sub keys.
- "data"

The key "*" is predefined as default value of its parent key.

The key "timestamp" will be updated automatically after its sibling key changing.

A key is also called a "node". You can use -g or --go argument to navigate to a node, and then can list  , add , delete, list its sub nodes, or set value for its sub nodes.

### usage

Here is a detailed usage example :

```
mynote.py  #enter an interactive mode

#>>-h      #show command help

#>> -o test.data -p 123456  #open or create a new data file. a password is needed.

mynote:/>>-g .  #list sub nodes
  +?:meta:...
  +?:data:...
  
mynote:/>>-g m   #enter the "meta" node.Nodename can be a fuzzy matching.
  -=:name:mynote
  -=:timestamp:2022-04-09 09:04:39
  
mynote://meta>>-g ..  #enter parrent node.
  +?:meta:...
  +?:data:...
  
mynote:/>>-g d      #enter the "data" node

mynote://data>>--addkey sites   #add a sub node

mynote://data>>-g si  #enter the node "sites"

mynote://data/sites>>--setval http://a.com --name a #set value for sub node "a"
mynote://data/sites>>--setval http://b.com --name a #set value for sub node "b"

mynote://data/sites>>-g .    //list sub nodes
  -=:a:http://a.com
  -=:timestamp:2022-04-09 09:29:50
  -=:b:http://b.com
  
mynote://data/sites>>-q   //quit and save
confirm to save(y|n):y

mynote.py --genpwd 16 --noloop  //generate a random string of 16 chars.
\L[VzJlOv$@LnQT;
```



Here  are details of some arguments:

**--go .**   #list sub nodes. Output my be like:

​	+?: a : ...            // "+" means "a" has sub nodes, “...” means "a"  has no default value.

​	+*: b: b-value    //  "\*" means  "b" has default value "b-value" , which the node name is "\*".

​	-=: c: c-value     // "-" means "c" has no sub nodes. "=" means "c-value" is the value of "c".

### Encryption Algorithm

password: any string provided by user.

key: key=sha256(password)

encryption: encrypted_data=AES(key, plain_data)








import json
import os
import time
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
import random
import sys, getopt

class MyNote:
    '''MyNote deals with a notebook file that sotres user data.
    The user data is a json string of specified format,which is hold by the property jsonObj of MyNote object.
    The user data is encrypted when saved to file, and decrypted when read from file.
    So the user data is safe even after the data file is stoled by others because the data file is encrypted.
    '''
    fpath=None
    '''the path of user data file'''
    jsonObj={}
    '''an dict object that holds the user data.'''
    _password_:str=""
    '''password to the user data file.'''
    _key_:bytes=b""
    '''key to encrypt and decrypt the user data.It's caculated automatically.'''

    cipher=None
    '''Cipher object'''

    selected_path=[]
    '''path to current node index.It's a list of node name.Eg:['data','item1']'''
    __selected_node__=None

    def check_format(self):
        if self.jsonObj:pass
        if self.jsonObj['meta']:pass
        if self.jsonObj['meta']['name']:pass
        if self.jsonObj['meta']['timestamp']:pass
        if self.jsonObj['data']:pass

    def __init__(self,fpath="mynote.data",password="1234567812345678"):
        '''get a MyNote object, which loads data from fpath or creates a new one if fpath dosen't exit. 
        fpath:data file path.
        '''
        self.fpath=fpath
        self._password_=password
        self.password_to_key()

        self.cipher=AES.new(self._key_,AES.MODE_ECB)

        if os.path.exists(fpath):
            with open(fpath,'rb') as f:
                cipher_text=f.read()
                
                try:

                    plain_text=self.cipher.decrypt(cipher_text).decode('utf-8')
                    self.jsonObj=json.loads(plain_text)
                    self.check_format()
                except Exception as ex:
                    print("fail to decipher data file.")
                    return 
                
                
        else:
            self.jsonObj=self.create_note()
            self.save_note()
        self.navigate()
    
    def password_to_key(self):
        '''caculate a key to encrypt and decrypt user data according to password.'''
        ho=SHA256.new(self._password_.encode('utf-8'))
        self._key_=ho.digest()

    def get_padded_bytes(self):
        '''pad  the user data to cipher block_size. '''
        blksize=self.cipher.block_size
        plain_bytes=json.dumps(self.jsonObj).encode('utf-8')
        padding_len=blksize-(len(plain_bytes) % blksize )
        if padding_len<blksize:
            plain_bytes=plain_bytes+b' '*padding_len
        
        return plain_bytes

    def create_note(self,note_name="mynote"):
        note={"meta":{},"data":{}}
        note["meta"]["name"]=note_name
        note["meta"]["timestamp"]=time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())
        return note
    
    def get_selected_path(self):
        s=''
        for x in self.selected_path:
            s=s+'/'+x
        return s

    def addkey(self,name):
        '''add a sub node in current selected node.'''
        if name==None or name=='' :return
        if name in self.__selected_node__.keys():
            print("error:%s already exists."%(name))
            return
        else:
            self.__selected_node__[name]={}
        self.__selected_node__["timestamp"]=time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())


    def delkey(self,name):
        '''delete a sub node in current selected node.'''
        if name==None:return
        if len(self.selected_path)<1:
            print("can't delete root node.")
            return
        if name in self.__selected_node__.keys():
            x=input("confirm to delete %s (y|n):"%(name))
            if x!='y':return             
            del self.__selected_node__[name]
        self.__selected_node__["timestamp"]=time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())

    
    
    def setval(self,val,name='*'):
        '''Set value for a property of node.The default property is named as '*'.
        val:property value.
        name:property name,default to '*'.
        '''
        if name in ('',None):name='*'
        if val==None :
            print('property value or name can\'t be None.')
            return
        if name in self.__selected_node__.keys():
            '''if name is a dict,then set it's default value*.'''
            if type(self.__selected_node__[name])==dict:
                self.__selected_node__[name]['*']=val            
        self.__selected_node__[name]=val
        self.__selected_node__["timestamp"]=time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())

    
    def navigate(self,path='.'):
        '''navigate to a node.
        path:.|..|nodename,it's a relative path.
        '''
        if path in (None,''):return     

        if path=='..':
            if len(self.selected_path)>0:
                self.selected_path.pop()
        elif path=='.':
            pass
        else:
            ks=self.search(path)
            if len(ks)==1:
                path=ks[0]
            elif len(ks)>1:
                print("do you mean?")
                for x in ks:print("  ",x)
                return "more"

            if path not in self.__selected_node__.keys():
                return
            if type(self.__selected_node__[path]) != dict:
                return 
            self.selected_path.append(path)

        self.__selected_node__=self.jsonObj

        for ki in self.selected_path:
            try:
                self.__selected_node__=self.__selected_node__[ki]                
            except Exception as ex:
                print(ex)
    
    def decrypt_to(self,path=None):
        '''decrypt current data to a file of palin text.'''
        if path==None or path=='':
            path=self.fpath+'.json'
        plain_text=json.dumps(self.jsonObj)
        with open(path,'w',encoding='utf-8') as f:
            f.write(plain_text)
        print("decrypted to %s\n"%(path))

    def encrypt_to(self,path=None):
        '''encrypt current data to a file of encrypted data.'''
        if path==None or path=='':
            path=self.fpath+'.data'
        plain_text=json.dumps(self.jsonObj).encode('utf-8')
        cipher_text=self.cipher.encrypt(self.get_padded_bytes())
        with open(path,'wb') as f:
            f.write(cipher_text)

        print("encrypted to %s\n"%(path))

    def save_note(self,fpath=None):
        if fpath==None:
            fpath=self.fpath
        x=input("confirm to save(y|n):")
        if x!='y':
            return
        self.jsonObj['meta']["timestamp"]=time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())      
        
        plain_text=json.dumps(self.jsonObj).encode('utf-8')        
        cipher_text=self.cipher.encrypt(self.get_padded_bytes())
        with open(self.fpath,'wb') as f:
            f.write(cipher_text)
    
    def load_from(self,path):
        '''load data from a plain text json file.'''
        s=''
        with open(path,'r',encoding='utf-8') as f:
            s=f.read()
        try:
            self.jsonObj=json.loads(s)
        except Exception as ex:
            print("not a valid json file")

    def search(self,keyword=''):
        '''search sub nodes in current node.'''
        if keyword==None:return []
        ks=[]
        for ki in self.__selected_node__.keys():
            if keyword in ki:
                ks.append(ki)
            if keyword==ki:
                ks.clear()
                ks.append(ki)
                break
        return ks
    def gen_safe_password(n=16):
        '''generate a random password string.
        n:password length'''
        if n in ('',None):n=16
        if type(n)==str:n=eval(n)
        
        cs="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+-*/\~`!@#$%^&()-[]{};:\"'<,>.?"
        pwd=[]
        for i in range(0,n):
            c=cs[random.randint(0,len(cs)-1)]
            pwd.append(c)
        return ''.join(pwd)

def get_opt_value(opt,opts):
    '''search option value in opts
       opt:tuple('-i','--input')
       opts:tuple(('opt1','value1'),('opt2','value2')...)
    '''
    for o,v in opts:
        if o in opt:            
            return v
    return None



mynote:MyNote=None

def main(argv,loop=True):
    '''handle command line agruments.
    loop:True to enter into a cmd loop that handles arguments interactively.
    '''
    global mynote
    try:
        opts,argvs=getopt.getopt(argv,"hqo:p:a:g:",["help","open=","pass=", \
            "addkey=","setval=","name=","dump","save","cd=","decrypt=","encrypt=", \
                "loadfrom=","go=","delkey=","noloop","genpwd="])
        
    except getopt.GetoptError as ex:
        print("error command,see:\n  mynote.py -h")
        return
    for opt,arg in opts:
        if opt in ('-h','--help'):
            print("mynote by ShadowlessWalker.")
            print("-o x.data -p 123456  #open an encrypted data file")
            print("-g|--go .|..|nodename #navigate to a node.")
            print("--dump  #dump all data")
            print("--addkey nodename")
            print("--delkey nodename")
            print("--setval value [--name nodename]  #set a value for a node.")
            print("--decrypt ofile #decrpt data to a file")
            print("--encrypt ofile #encrypt data to a file")
            print("--loadfrom ifile  #load data from an unencrypted file")
            print("-q  #quit")
            print("--noloop  #quit after handling the arguments.")
            print("--genpwd n  #generate a n-chars random string.")
        elif opt in ('-q'):
            if mynote:
                mynote.save_note()
            sys.exit(0)
        elif opt in ('-o','--open'):
            try:                
                mynote=MyNote(arg,get_opt_value(('-p','--pass'),opts))                             
            except Exception as ex:
                print(ex)
                return
              
        elif opt in ('--addkey'):
           mynote.addkey(get_opt_value(('--addkey'),opts))
        elif opt in ("--delkey"):
            mynote.delkey(get_opt_value(("--delkey"),opts))
        elif opt in('--setval'):
            v=get_opt_value(('--setval'),opts)
            k=get_opt_value(('--name'),opts)
            mynote.setval(v,name=k)


        elif opt in ('-d','--dump'):
            print(mynote.jsonObj)
        elif opt in ('-s','--save'):
            mynote.save_note()
        elif opt in ('--cd'):
            mynote.navigate(get_opt_value(('--cd'),opts))
        elif opt in ('--decrypt'):
            mynote.decrypt_to(path=get_opt_value(('--decrypt'),opts))
        elif opt in ('--encrypt'):
            mynote.encrypt_to(path=get_opt_value(('--encrypt'),opts))
        elif opt in ('--loadfrom'):
            mynote.load_from(path=get_opt_value(('--loadfrom'),opts))
        elif opt in ('--go',"-g"):
            '''navigate to a node'''
            k=get_opt_value(('--go'),opts)
            
            if mynote.navigate(k)=='more':return

            if type(mynote.__selected_node__) ==dict:
                for ki in mynote.__selected_node__.keys():
                    v=mynote.__selected_node__[ki]
                    if type(v)==dict:
                        if "*" in v.keys():
                            print("  +*:%s:%s"%(ki,v['*']))
                        else:
                             print("  +?:%s:..."%(ki))
                    else:
                        print("  -=:%s:%s"%(ki,v))                   
            else:
                print("  -=:%s"%(mynote.__selected_node__))
        elif opt in ('--noloop'):
            loop=False
        elif opt in ('--genpwd'):
            n=get_opt_value(('--genpwd'),opts)
            print(MyNote.gen_safe_password(n))

    return loop


if __name__=="__main__":
    argv=sys.argv[1:]
    if main(argv)==False:exit(0)

    while True:
        if mynote!=None:
            argv=input("%s:/%s>>"%(mynote.jsonObj['meta']['name'],mynote.get_selected_path()))
        else:
            argv=input("#>>")        
        main(argv.split(' '))

